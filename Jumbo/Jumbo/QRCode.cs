﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbo
{
    public class QRCode
    {
        private string str;

        public QRCode(string str)
        {
            this.str = str;
        }
        public Image Image { get; set; }


        public override string ToString()
        {
            return str;
        }
    }
}
