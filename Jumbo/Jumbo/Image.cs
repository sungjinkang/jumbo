﻿using System.Collections.Generic;

namespace Jumbo
{
    public class Image
    {
        public IEnumerable<byte> Bytes { get; set; }
    }
}