﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jumbo.UnitTests
{
    [TestClass]
    public class JumboTests
    {
        [TestMethod]
        public void CreateNew_EmptyString_EmptyResponse()
        {
            var qrcode = new QRCode(string.Empty);

            Assert.AreEqual(null, qrcode.Image);
            Assert.AreEqual(string.Empty, qrcode.ToString());
        }
    }
}
